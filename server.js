var express  = require('express');
const path = require('path');
var logger = require('morgan');
var bodyParser = require('body-parser');
var cors = require('cors');
const config = require('config');
//var route = require('./config/databases');
var route = require('./app/routes');


var app  = express();
var port = config.get('express_port'); // http

app.use(bodyParser.urlencoded({ extended: false })); // Parses urlencoded bodies
app.use(bodyParser.json()); // Send JSON responses
app.use(logger('dev')); // Log requests to API using morgan
app.use(cors());
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});
 
app.use('/', route);

// start web server
app.listen(port);
console.log(`App started on port : ${port}`);

module.exports = app;
