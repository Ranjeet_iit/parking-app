const mongoose = require('mongoose');

mongoose.Promise = global.Promise;
const dbURI = 'mongodb://user:vishal123@ds149593.mlab.com:49593/user',[user];

mongoose.connect(dbURI);

mongoose.connection.on("connected",function(){
    console.log('Mongoose default connection open to ' + dbURI);
})

mongoose.connection.on('error', function(err){
    console.log('Mongoose default connection error: ' + err)
});

mongoose.connection.on('disconnected', function () {  
    console.log('Mongoose default connection disconnected'); 
});

// If the Node process ends, close the Mongoose connection 
process.on('SIGINT', function() {  
    mongoose.connection.close(function () { 
    console.log('Mongoose default connection disconnected through app termination'); 
    process.exit(0); 
    }); 
}); 

module.exports = mongoose;